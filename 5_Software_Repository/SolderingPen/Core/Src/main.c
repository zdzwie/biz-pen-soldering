/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "tim.h"
#include "gpio.h"
#include "cstdio"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "ssd1306.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

typedef enum{
	NORMAL,
	STANDBY,
	SMALL,
	LONG,
	TIP_OFF
} states;
 states state;
		

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM14)
	{
		// count to 1 minute
		state = STANDBY;
	}
	
	if(htim->Instance == TIM17)
	{
		// count to 8 minute
		// wylaczanie lutownicy
		}
}
int32_t Read_Temperature();				// odczyt temperatury z termopary
int32_t Read_Set_Temperature();		// odczyt z potencjometru i ustawienie temperatury
int32_t TIP_CHECK();							// sprawdzenie czy grot jest podlaczony

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

	int set_temperature = 0;
	int temperature = 0;
	char TEMP[20];
	
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_TIM14_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */

	SSD1306_Init();
	
	SSD1306_Fill(0);
	SSD1306_UpdateScreen();
	
	SSD1306_UpdateScreen();
	
	if(TIP_CHECK() == 1)
		 state = NORMAL;
	else 
		state = TIP_OFF;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		set_temperature = Read_Set_Temperature();
		temperature = Read_Temperature();
			switch(state)
		{
			case NORMAL:
			if(abs(set_temperature - temperature ) < 1)
			{
				HAL_TIM_Base_Start_IT(&htim14);
				state = SMALL;
			}
			
			//PID
			
						SSD1306_GotoXY(10,10);
			SSD1306_Puts("NORMAL", &Font_11x18,1);
				break;
			case STANDBY:
				//temperatura do 180 C
			
			if(abs(Read_Set_Temperature() - Read_Temperature() ) < 1)
			{
				HAL_TIM_Base_Start_IT(&htim17);
				state = LONG;
			}
						SSD1306_GotoXY(10,10);
			SSD1306_Puts("STANDBY", &Font_11x18,1);
				break;
			case SMALL:
				if(abs(Read_Set_Temperature() - Read_Temperature() ) > 1)
				{
					state = NORMAL;
					HAL_TIM_Base_Stop_IT(&htim14);
					HAL_TIM_Base_Stop_IT(&htim17);
				}
							SSD1306_GotoXY(10,10);
			SSD1306_Puts("SMALL", &Font_11x18,1);
				break;
			case LONG:
							SSD1306_GotoXY(10,10);
			SSD1306_Puts("LONG", &Font_11x18,1);
				if(abs(Read_Set_Temperature() - Read_Temperature() ) > 1)
				{
					state = NORMAL;
					HAL_TIM_Base_Stop_IT(&htim14);
					HAL_TIM_Base_Stop_IT(&htim17);
				}
				break;
			case TIP_OFF:
				state = STANDBY;
			SSD1306_GotoXY(10,10);
			SSD1306_Puts("TIP_OFF", &Font_11x18,1);
				break;
		}
		
		sprintf(TEMP, "%i",temperature);
		SSD1306_GotoXY(10,30);
		SSD1306_Puts(TEMP, &Font_11x18,1);
		
		if(TIP_CHECK() == 1)
			state = NORMAL;
		else 
			state = TIP_OFF;
		
    /* USER CODE END WHILE */

	
		
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
int32_t Read_Set_Temperature()
{
	
}

int32_t Read_Temperature()
{
	
}

int32_t TIP_CHECK()
{
	
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
